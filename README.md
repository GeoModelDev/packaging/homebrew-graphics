# homebrew-graphics

The repository hosts Homebrew formulas to install the graphics libraries needed by the GeoModel packages.


In particular, some of the packages of the [Coin3D suite](https://github.com/coin3d):

- Simage -- a library to handle graphics files
- Coin -- the 3D graphics engine
- SoQt -- a glue library between Coin and the windowing system Qt5

_Note:_ If you do not have `brew` installed on your machine, you can find documentation and installation instructions on the 'Homebrew' website: <https://brew.sh/>
