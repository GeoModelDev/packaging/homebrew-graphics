class SoqtBb < Formula
  desc "The SoQt package, allowing Coin to be used with Qt"
  homepage "https://bitbucket.org/Coin3D/SoQt"

  # SOURCES
  #url "https://bitbucket.org/rmbianchi/soqt/get/mac-mojave-fix.zip"
  url "https://bitbucket.org/rmbianchi/soqt/downloads/soqt-1b4fe9d-macos-mojave-fix.zip"
  sha256 "1649c25c660d1bfa5e698de5ceffd470483a2dc26f55c85cdd8bc2164b0b0814"

  head "https://bitbucket.org/rmbianchi/soqt",
      :using    => :hg

  # DEPENDENCIES
  depends_on "cmake" => :build
  depends_on "qt5"
  depends_on "atlas/graphics/coin-bb"

  # BUILD INSTRUCTIONS
  def install
    mkdir "builddir" do
      system "cmake", "..", *std_cmake_args
      system "make"
      system "make", "install"
    end
  end

  # PRE-COMPILED PACKAGES
  bottle do
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "9aaebdd86e6f50295cce21b101cedc50648c8db2e7f2dd0c0b31093961cd2bf3" => :mojave
  end

end
