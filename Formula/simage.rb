class Simage < Formula
  desc "The simage library for texture mapping in 3D graphics applications"
  homepage "https://grey.colorado.edu/coin3d/index.html"

  # SOURCES 
  url "https://www.qat.pitt.edu/simage-1.7.1.tar.gz"
  sha256 "87db05a1b3ed2d9971eb29fcca6b8074ef20231a7679a5de36faf49ca0304334"

  # COMPILATION INSTRUCTIONS
  def install
    system "./configure", "--disable-debug",
                          "--disable-dependency-tracking",
                          "--disable-silent-rules",
                          "--prefix=#{prefix}"
    system "make", "install" # if this fails, try separate make/make install steps
  end

  # PRE-COMPILED PACKAGES (Bottles)
  bottle do
    ## root_url "https://qat.pitt.edu/Bottles"
    ## cellar :any
    ## sha256 "9b64635cb7928ea0cbe77b55bc2fde45330096eb05fc9d65eac20ce6ec3db07b" => :high_sierra
    root_url "http://atlas-vp1.web.cern.ch/atlas-vp1/sources/bottles"
    cellar :any
    sha256 "7389e4de6a3724df9df5375c3b12f59bb65dead5bdcb19bb61a2c315a3c6fc10" => :mojave
  end

end
